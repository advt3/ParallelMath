# Parallel Math
An instructional project to present concepts of parallel programming for C++, Cuda with a Python interface.

## Dependencies
The project has been developed using ubuntu 20.04, details can be found on this [development environment post](https://advt3.com/posts/development_environment/)
- Docker >= 1.9
- Boost >= 1.71
- Python >= 3.6
- CMake >= 3.5 
- Cuda >= 10.2
- Gcc/G++ >= 5.4

## Setup
- Run the [build script](./build.sh)
- Execute unit test with:
    
        sudo docker run -it --rm --gpus all parallel_math

## Results
The results should look something like this

```
Test project /opt/parallel_math/build
    Start 1: multithread_natural_numbers_tests
1/6 Test #1: multithread_natural_numbers_tests ...   Passed    0.00 sec
    Start 2: multithread_vector_tests
2/6 Test #2: multithread_vector_tests ............   Passed    3.12 sec
    Start 3: multithread_math_python_test
3/6 Test #3: multithread_math_python_test ........   Passed    0.02 sec
    Start 4: parallel_natural_numbers_tests
4/6 Test #4: parallel_natural_numbers_tests ......   Passed    0.61 sec
    Start 5: parallel_vector_tests
5/6 Test #5: parallel_vector_tests ...............   Passed    0.28 sec
    Start 6: parallel_math_python_test
6/6 Test #6: parallel_math_python_test ...........   Passed    0.16 sec

100% tests passed, 0 tests failed out of 6

Total Test time (real) =   4.21 sec
```

## License
MIT License Copyright (c) 2020 Santiago Hurtado