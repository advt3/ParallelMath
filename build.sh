#!/bin/bash
[ ! -f ./boost_1_71_0.tar.gz ] && wget https://dl.bintray.com/boostorg/release/1.71.0/source/boost_1_71_0.tar.gz
sudo docker build . -t parallel_math
